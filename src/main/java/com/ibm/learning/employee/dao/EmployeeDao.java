package com.ibm.learning.employee.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibm.learning.employee.entities.Employee;

@Repository
public interface EmployeeDao extends CrudRepository<Employee, Integer>{

}
