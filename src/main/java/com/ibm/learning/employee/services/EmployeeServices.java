package com.ibm.learning.employee.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.learning.employee.dao.EmployeeDao;
import com.ibm.learning.employee.entities.Employee;

@Service
public class EmployeeServices {
	@Autowired
	EmployeeDao employeeDao;

	public Employee createEmployee(Employee employee) {
		return employeeDao.save(employee);
	}

	public List<Employee> getListOfEmployees() {

		 Iterable<Employee> allEmployees = employeeDao.findAll();
		 List<Employee> employeeList = new ArrayList<>();
		 allEmployees.forEach(employeeList::add);
		 return employeeList;
	}

	public Employee getEmployeeById(Integer id) {
		return employeeDao.findById(id).orElse(null);

	}

	public Employee updateEmployee(Employee employee) {

		Employee employeeUpdated = employeeDao.findById(employee.getEmployeeId()).orElse(new Employee());

		employeeUpdated = employee;
		return employeeDao.save(employeeUpdated);
	}

	public void deleteEmployeebyId(Integer id) {

		employeeDao.deleteById(id);
	}

}
